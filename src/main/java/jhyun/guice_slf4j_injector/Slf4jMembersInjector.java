package jhyun.guice_slf4j_injector;

import java.lang.reflect.Field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.MembersInjector;

public class Slf4jMembersInjector<T> implements MembersInjector<T> {
	private final Field field;
	private final Logger logger;

	public Slf4jMembersInjector(Field field) {
		super();
		this.field = field;
		this.logger = LoggerFactory.getLogger(field.getDeclaringClass());
		this.field.setAccessible(true);
	}

	public void injectMembers(T arg0) {
		try {
			field.set(arg0, logger);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

}
