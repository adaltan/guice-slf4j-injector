package jhyun.guice_slf4j_injector;

import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;

public class Slf4jModule extends AbstractModule {

	@Override
	protected void configure() {
		bindListener(Matchers.any(), new Slf4jTypeListener());
	}

}
