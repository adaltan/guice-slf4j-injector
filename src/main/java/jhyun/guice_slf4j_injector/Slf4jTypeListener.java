package jhyun.guice_slf4j_injector;

import java.lang.reflect.Field;

import org.slf4j.Logger;

import com.google.inject.TypeLiteral;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;

public class Slf4jTypeListener implements TypeListener {

	public <I> void hear(TypeLiteral<I> aTypeLiteral,
			TypeEncounter<I> aTypeEncounter) {
		Field[] fields = aTypeLiteral.getRawType().getDeclaredFields();
		for (int n = 0; n < fields.length; n++) {
			Field field = fields[n];
			if (field.getType() == Logger.class
					&& field.isAnnotationPresent(InjectLogger.class)) {
				aTypeEncounter.register(new Slf4jMembersInjector<I>(field));
			}
		}
	}

}
