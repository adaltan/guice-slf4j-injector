package jhyun.guice_slf4j_injector;

import junit.framework.Assert;

import org.junit.Test;
import org.slf4j.Logger;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class Slf4jModuleTestCase {

	@InjectLogger
	private Logger log;

	private void injectThis() {
		Injector injector = Guice.createInjector(new Slf4jModule());
		injector.injectMembers(this);
	}

	@Test
	public void test() {
		//
		this.injectThis();
		//
		Assert.assertNotNull("why u no inject-logger?!", log);
		this.log.debug("well, ok...");
	}

}
